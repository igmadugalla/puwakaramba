module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
   // CONFIG ===================================/

    watch: {
      compass: {
        files: ['styles/**/*.{scss,sass}'],
        tasks: ['compass:dev']
      },
      js: {
        files: ['js/*.js'],
        tasks: ['uglify']
      },
      css: {
        files: ['styles/css/*.css'],
        tasks: ['cssmin']
      }
    },
    compass: {
      dev: {
        options: {              
          sassDir: ['styles/sass'],
          cssDir: ['styles/css'],
          environment: 'development'
        }
      },
    },
    uglify: {
      all: {
        files: {
            'js/dist/main.min.js': [
            'js/jquery-1.12.1.min.js',
            'js/jquery.themepunch.plugins.min.js',
            'js/jquery.themepunch.revolution.min.js',
            'js/scripts.js',
            ]
          }
      },
    },
    cssmin: {
      dist: {
        options: {
           banner: '/*! MyLib.js 1.0.0 | Aurelio De Rosa (@AurelioDeRosa) | MIT Licensed */'
        },
        files: {
           'styles/dist/main.min.css': ['styles/css/normalize.min.css','styles/css/main.css','styles/css/settings.css']
        }
      }
    }

  });

  // DEPENDENT PLUGINS =========================/

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // TASKS =====================================/

  grunt.registerTask('default', ['compass:dev' , 'uglify' , 'watch', 'cssmin']);

};