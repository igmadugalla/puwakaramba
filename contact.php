<!doctype html>
<html lang="en-US" class="no-js">
	<head>
		<meta charset="UTF-8">
		<title>Welcome</title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    	<link href="img/favicon.png" rel="shortcut icon">

    	<link rel="stylesheet" type="text/css" href="styles/dist/main.min.css">
    	
	</head>
	<body>
		<div class="wrapper">
			<header class="header-section">
				<div class="logo-header">
					<div class="container">
						<div class="logo-cols">
							<a class="logo" href="">
								<img src="img/logo.png" alt="">
							</a>
						</div>
						<div class="contact-cols">
							<ul>
								<li> <i class="contact-icon tp"></i> 0112 656 560</li>
								<li> <i class="contact-icon fax"></i> 0112 656 562</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="nav-header">
					<div class="container">
						<nav class="navigation">
							<ul>
								<li class="active"><a href="#">Home</a></li>
								<li><a href="#">About</a></li>
								<li>
									<a href="#">Products</a>
									<ul class="submenu">
										<li><a href="#">Plywood</a></li>
										<li><a href="#">MDF</a></li>
										<li><a href="#">Hardboard</a></li>
										<li><a href="#">Particle Board ( Chipboard )</a></li>
										<li><a href="#">Upholstery Fabric</a></li>
										<li><a href="#">Furniture Hardware</a></li>
										<li><a href="#">Screws</a></li>
										<li><a href="#">Edge Banding</a></li>
									</ul>
								</li>
								<li><a href="#">Vedio</a></li>
								<li><a href="#">Contacts us</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</header>
			<div class="cover-section">
				<div class="contact-cover-section">
					<div class="map-container">
						<div class="overlay"><div class="icon"></div></div>
						<div class="map-wrap" id="map"></div>
					</div>
					<div class="container">
						<div class="cover-inner">
							<h1>Get in Touch</h1>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
							</p>
							<div class="contact-form">
								<form action="">
									<div class="form-row">
										<div class="col-12">
											<div class="form-group">
												<input type="text" name="name" id="name" placeholder="Name">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-6 col-mb-12">
											<div class="form-group">
												<input type="text" name="name" id="name" placeholder="Name">
											</div>
										</div>
										<div class="col-6 col-mb-12">
											<div class="form-group">
												<input type="text" name="phone" id="phone" placeholder="Phone Number">
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-12 col-mb-12">
											<div class="form-group">
												<textarea name="message" id="message" placeholder="Message"></textarea>
											</div>
										</div>
									</div>
									<div class="form-row">
										<div class="col-12 col-mb-12">
											<div class="form-group">
												<button class="contact-btn"><span>Submit</span></button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="body-section">
				<div class="row row-contact">
					<div class="container">
						<div class="col-4 contact-box">
							<i class="con-icon location"></i>
							<p>No. 05 , Moratumulla South , Moratuwa.</p>
						</div>
						<div class="col-2 contact-box">
							<i class="con-icon tp"></i>
							<p>0112 656 560</p>
						</div>
						<div class="col-2 contact-box">
							<i class="con-icon fax"></i>
							<p>0112 656 562</p>
						</div>
						<div class="col-4 contact-box">
							<i class="con-icon email"></i>
							<p>info@puwakarambaent.com / sandj@sltnet.lk </p>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer-section">
				<div class="container">
					<div class="row">
						<div class="col-4">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="footer-about">About</h3>
								<div class="containt-slide footer-about">
									<p>
										PUWAKARAMBA ENTERPRISES is an organization with a highly reputation of  importer and major distributor in Sri Lanka. We are mainly importing the Wood panel products such as 
										Plywood, MDF, Hardboard with all kind of Hardware items and Upholstery Fabric etc..
										<br><br>
										During these 15 years of business experience we have assembled valuable customer base country wide and as that we have supplied our products to all provincial areas.
									</p>
								</div>
							</div>
						</div>
						<div class="col-3">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="footer-about">Head Office</h3>
								<div class="containt-slide ">
									<p>
										Address: No 44 , De Soysa Road, <br>
										Rawathawatte, Moratuwa
										<br><br>
										Tel: 011- 2648047  , Fax : 011 2648048 <br>
										<br><br>
										E-mail: sandj@sltnet.lk <br>
										info@puwakarambaent.com
										<br><br>
										Show Room Address: Puwakaramba Enterprises,<br>
										Moratumulla South, Moratuwa, <br> Sri Lanka
										<br><br>
										Tel: 011-2654084 - 011-2653664 <br>
										Fax - 011-2648048
										<br><br>
										Warehouse
										<br><br>
										Address : No. 1 / 3 , Polhena ,Madapatha.
										<br><br>
										Tel: 0112706381 <br>
										Fax : 011 2706380
									</p>
								</div>
							</div>
						</div>
						<div class="col-2">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="">About</h3>
								<div class="containt-slide ">
									<ul class="social-icon">
										<li><a href="" target="_blank" class="icon-social icon-social-twitter"></a></li>
										<li><a href=""  target="_blank" class="icon-social icon-social-facebook"></a></li>
										<li><a href="#" class="icon-social icon-social-gplus"></a></li>
									</ul>

									<p>
										Terms and Conditions  
										Privacy  Disclaimer  
										Useful Links
									</p>

									<img class="qr-img" src="img/qr_code.jpg" alt="">

								</div>
							</div>
						</div>
						<div class="col-3">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="">STAY IN TOUCH WITH</h3>
								<div class="containt-slide ">
									<p>
										Sign up for the Leisure Travel Vans newsletter and be the first to know about news, videos, product updates, and more. You can unsubscribe at any time.
									</p>
								</div>
							</div>
							<div class="cols-rows">
								<h3 class="footer-click" data-click="">Newsletter</h3>
								<div class="containt-slide ">
									<p>
										Signup for the monthly newsletter to get usefull information for your busi
									</p>

									<form class="subscribe" action="">
										<input type="text" placeholder="Email Address">
										<button>Sign up</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="footer-line"></span>
			</footer>
		</div>
		<script type="text/javascript" src="js/dist/main.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdkKWC1E8QYIRsBoob4JAmnVeRYWGwxws&signed_in=true&callback=initMap&libraries=places"></script>
		<script>


var map;
function initialize() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 18,
    //6.7824784,79.8964392
    //6.7825423,79.8931777
    center: new google.maps.LatLng(6.7825423,79.8931777),
    mapTypeId: 'roadmap',
    styles: [
         {"elementType":"labels.icon","stylers":[{"visibility":"off"}]}
    ]
    
  });

  // var iconBase = base+'/img/loc/';
  // var icons = {
  //   logo: {
  //     icon: iconBase + 'tridel.png'
  //   }
  // };

  function addMarker(feature) {
    var marker = new google.maps.Marker({
      position: feature.position,
      //icon: icons[feature.type].icon,
      map: map
    });
  }

  var features = [
  
    {
      position: new google.maps.LatLng(6.782460, 79.896496),
      type: 'logo',
    }
    
  ];

  for (var i = 0, feature; feature = features[i]; i++) {
    addMarker(feature);
  }
}

google.maps.event.addDomListener(window, 'load', initialize);




	        </script>
	</body>
</html>	