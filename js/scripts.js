(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		// DOM ready, take it away

		//alert("ok ");

		var revapi;
	   	revapi = jQuery('.tp-banner').revolution(
		{
			delay:15000,
			startwidth:1170,
			startheight:500,
			hideThumbs:10,
			fullWidth:"off",
			fullScreen:"on",
			fullScreenOffsetContainer: ""

		});


		jQuery('.products-navigation .pro-controls').click(function(){
			//event.preventDefault();
			var path = jQuery(this).attr('title');
			
            if (path=='Right') {
            	//alert('Right');
                jQuery('#tabs li:first').detach().appendTo('ul#tabs').removeAttr('style');
            }
            if (path=='Left') {
            	//alert('Left');
                jQuery('#tabs li:last').detach().prependTo('ul#tabs').removeAttr('style');
            }

    	});


    	jQuery('.sub-navigation .tab-links li a').on('click', function(e)  {

    		if($(this).attr('class') != 'active')  {

    			jQuery('.sub-navigation .tab-links li a').removeClass('active');

	    		$(this).addClass('active');
	    		
		        var currentAttrValue = jQuery(this).attr('href');
		        //var currentimg = jQuery(this).attr('data-img');
		 		//alert(currentAttrValue);
		        // Show/Hide Tabs
		        jQuery('.products-containts ' + currentAttrValue).siblings().hide().removeClass('veiw-element');


		        jQuery('.loading').show();

		        setTimeout(function(){  
		        	jQuery('.loading').hide();
		        	jQuery('.products-containts ' + currentAttrValue).show().addClass('veiw-element');
		        }, 600);
		 
		        // Change/remove current tab to active
		        //jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

		        //jQuery('.tabs .tab-links li.active a img').attr('src',currentimg);
		 
		        e.preventDefault();
		    }
	    });


	    var vid = document.getElementById("bgvid"),
		pauseButton = document.getElementById("vidpause");
		function vidFade() {
		    vid.classList.add("stopfade");
		}
		vid.addEventListener('ended', function() {
		    // only functional if "loop" is removed 
		     vid.pause();
			// to capture IE10
			vidFade();
			$('.vedio-wrap').hide();
			$('.main-slider').css('left','0');
		});
		pauseButton.addEventListener("click", function() {
		    vid.classList.toggle("stopfade");
			if (vid.paused) {
				vid.play();
				pauseButton.innerHTML = "Pause";
			} else {
		        vid.pause();
		        pauseButton.innerHTML = "Paused";
			}
		});
		
	});
	
})(jQuery, this);