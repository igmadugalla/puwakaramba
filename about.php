<!doctype html>
<html lang="en-US" class="no-js">
	<head>
		<meta charset="UTF-8">
		<title>Welcome</title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    	<link href="img/favicon.png" rel="shortcut icon">

    	<link rel="stylesheet" type="text/css" href="styles/dist/main.min.css">
    	
	</head>
	<body>
		<div class="wrapper">
			<header class="header-section">
				<div class="logo-header">
					<div class="container">
						<div class="logo-cols">
							<a class="logo" href="">
								<img src="img/logo.png" alt="">
							</a>
						</div>
						<div class="contact-cols">
							<ul>
								<li> <i class="contact-icon tp"></i> 0112 656 560</li>
								<li> <i class="contact-icon fax"></i> 0112 656 562</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="nav-header">
					<div class="container">
						<nav class="navigation">
							<ul>
								<li class="active"><a href="#">Home</a></li>
								<li><a href="#">About</a></li>
								<li>
									<a href="#">Products</a>
									<ul class="submenu">
										<li><a href="#">Plywood</a></li>
										<li><a href="#">MDF</a></li>
										<li><a href="#">Hardboard</a></li>
										<li><a href="#">Particle Board ( Chipboard )</a></li>
										<li><a href="#">Upholstery Fabric</a></li>
										<li><a href="#">Furniture Hardware</a></li>
										<li><a href="#">Screws</a></li>
										<li><a href="#">Edge Banding</a></li>
									</ul>
								</li>
								<li><a href="#">Vedio</a></li>
								<li><a href="#">Contacts us</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</header>
			<div class="cover-section">
				<div class="about-cover-section">
					<div class="container">
						<div class="cover-inner">
							<h1>About Us</h1>
							<p>
								<span>Puwakaramba Enterprises</span> is an organization with a highly reputation of importer and major distributor in Sri Lanka. We are mainly importing the Wood panel products such as Plywood, MDF, Hardboard with all kind of 
								Hardware items and Upholstery Fabric etc..
								<br><br>
								During these 15 years of business experience we have assembled valuable customer base country wide and as that we have supplied our products to all provincial areas.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="body-section">
				<div class="row row-partner">
					<div class="container">
						<div class="partner-box">
							<img src="img/pesgo.png" alt="">
						</div>
						<div class="partner-box">
							<img src="img/rhino.png" alt="">
						</div>
						<div class="partner-box">
							<img src="img/janco.png" alt="">
						</div>
						<div class="partner-box">
							<img src="img/sunply.png" alt="">
						</div>
						<div class="partner-box">
							<img src="img/rhino.png" alt="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="container">
						<div class="containt-wrap center-wrap">
							<h1>Who We Are</h1>
							<p>
								Puwakaramba Enterprises , has been doing in business <span>since 1998</span>…, and has successfully established  <br>
								a good name with our customers for over the last 15 years.
								<br><br>
								We carry a range of Hardware Items and Interior Products that are currently distributed to over 500 main dealers and  <br>
								thousands of customers throughout the country.
								<br><br>
								And the <span>S & J TRADE  INTERNATIONAL</span> company was established  in 2006 as the subsidiary company <br>
								of Puwakaramba Enterprises , Mainly for the purpose of direct Importing.
							</p>
						</div>
					</div>
				</div>
			</div>
			<footer class="footer-section">
				<div class="container">
					<div class="row">
						<div class="col-4">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="footer-about">About</h3>
								<div class="containt-slide footer-about">
									<p>
										PUWAKARAMBA ENTERPRISES is an organization with a highly reputation of  importer and major distributor in Sri Lanka. We are mainly importing the Wood panel products such as 
										Plywood, MDF, Hardboard with all kind of Hardware items and Upholstery Fabric etc..
										<br><br>
										During these 15 years of business experience we have assembled valuable customer base country wide and as that we have supplied our products to all provincial areas.
									</p>
								</div>
							</div>
						</div>
						<div class="col-3">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="footer-about">Head Office</h3>
								<div class="containt-slide ">
									<p>
										Address: No 44 , De Soysa Road, <br>
										Rawathawatte, Moratuwa
										<br><br>
										Tel: 011- 2648047  , Fax : 011 2648048 <br>
										<br><br>
										E-mail: sandj@sltnet.lk <br>
										info@puwakarambaent.com
										<br><br>
										Show Room Address: Puwakaramba Enterprises,<br>
										Moratumulla South, Moratuwa, <br> Sri Lanka
										<br><br>
										Tel: 011-2654084 - 011-2653664 <br>
										Fax - 011-2648048
										<br><br>
										Warehouse
										<br><br>
										Address : No. 1 / 3 , Polhena ,Madapatha.
										<br><br>
										Tel: 0112706381 <br>
										Fax : 011 2706380
									</p>
								</div>
							</div>
						</div>
						<div class="col-2">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="">About</h3>
								<div class="containt-slide ">
									<ul class="social-icon">
										<li><a href="" target="_blank" class="icon-social icon-social-twitter"></a></li>
										<li><a href=""  target="_blank" class="icon-social icon-social-facebook"></a></li>
										<li><a href="#" class="icon-social icon-social-gplus"></a></li>
									</ul>

									<p>
										Terms and Conditions  
										Privacy  Disclaimer  
										Useful Links
									</p>

									<img class="qr-img" src="img/qr_code.jpg" alt="">

								</div>
							</div>
						</div>
						<div class="col-3">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="">STAY IN TOUCH WITH</h3>
								<div class="containt-slide ">
									<p>
										Sign up for the Leisure Travel Vans newsletter and be the first to know about news, videos, product updates, and more. You can unsubscribe at any time.
									</p>
								</div>
							</div>
							<div class="cols-rows">
								<h3 class="footer-click" data-click="">Newsletter</h3>
								<div class="containt-slide ">
									<p>
										Signup for the monthly newsletter to get usefull information for your busi
									</p>

									<form class="subscribe" action="">
										<input type="text" placeholder="Email Address">
										<button>Sign up</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="footer-line"></span>
			</footer>
		</div>
		<script type="text/javascript" src="js/dist/main.min.js"></script>
	</body>
</html>	