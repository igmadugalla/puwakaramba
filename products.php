<!doctype html>
<html lang="en-US" class="no-js">
	<head>
		<meta charset="UTF-8">
		<title>Welcome</title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    	<link href="img/favicon.png" rel="shortcut icon">

    	<link rel="stylesheet" type="text/css" href="styles/dist/main.min.css">
    	
	</head>
	<body>
		<div class="wrapper">
			<header class="header-section">
				<div class="logo-header">
					<div class="container">
						<div class="logo-cols">
							<a class="logo" href="">
								<img src="img/logo.png" alt="">
							</a>
						</div>
						<div class="contact-cols">
							<ul>
								<li> <i class="contact-icon tp"></i> 0112 656 560</li>
								<li> <i class="contact-icon fax"></i> 0112 656 562</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="nav-header">
					<div class="container">
						<nav class="navigation">
							<ul>
								<li class="active"><a href="#">Home</a></li>
								<li><a href="#">About</a></li>
								<li>
									<a href="#">Products</a>
									<ul class="submenu">
										<li><a href="#">Plywood</a></li>
										<li><a href="#">MDF</a></li>
										<li><a href="#">Hardboard</a></li>
										<li><a href="#">Particle Board ( Chipboard )</a></li>
										<li><a href="#">Upholstery Fabric</a></li>
										<li><a href="#">Furniture Hardware</a></li>
										<li><a href="#">Screws</a></li>
										<li><a href="#">Edge Banding</a></li>
									</ul>
								</li>
								<li><a href="#">Vedio</a></li>
								<li><a href="#">Contacts us</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</header>
			<div class="cover-section">
				<div class="products-cover-section">
					<div class="container">
						<div class="cover-inner">
							<h1>Our Products</h1>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
							</p>
						</div>
						<div class="products-cover">
							<div class="box-main">
								<img src="img/1.jpg" alt="">
							</div>
							<div class="box-main box-sub">
								<img src="img/2.jpg" alt="">
							</div>
							<div class="box-main box-small-top">
								
							</div>
							<div class="box-main box-small-bottom">
								<img src="img/3.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="body-section">
				<div class="row row-products">
					<div class="container">
						<div class="products-navigation">
							<ul id="tabs">
								<li class="active"><a href="#">Plywood</a></li>
								<li><a href="#">Mdf</a></li>
								<li><a href="#">Hardboard</a></li>
								<li><a href="#">Particle board (Chipboard)</a></li>
								<li><a href="#">Plywood</a></li>
							</ul>
							<div class="pro-controls pro-prev" title="Left"></div>
							<div class="pro-controls pro-next" title="Right"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="container">
						<div class="containt-wrap">
							<div class="products-tab-master">
								<div class="sub-navigation">
									<ul class="tab-links">                          
										<li><a class="active" href="#list-1"><h6>Local Plywood</h6></a></li>
										<li><a href="#list-2"><h6>Commercial Plywood</h6></a></li>
										<li><a href="#"><h6>PVC Plywood</h6></a></li>
										<li><a href="#"><h6>Film Faced Plywood</h6></a></li>
									</ul>
								</div>
								<div class="products-containts">
									<div class="loading">
										<div class="loading-box"></div>
									</div>
									<!-- Slide 1 -->
									<div class="products-list veiw-element" id="list-1">
										<div class="row">
											<div class="col-7">
												<h1>Local Plywood</h1>
												<p>
													Our Plywood sheets are manufactured in Modemized Factories and are of best Quality that can be used for residential & commercial furniture products and in the construction industry for numerous applications.
												</p>

												<a href="#" class="btn-defult">How it is Made</a>
											</div>
											<div class="col-5 image-cols">
												<img src="img/product.jpg" alt="">
											</div>
										</div>
										<div class="row">
											<div class="heading">
												<h6>Features</h6>
											</div>
											<ul class="features">
												<li>
													<i class="features-icon dimension"></i>
													<p class="defult-font">Common Dimension is 1220 X 2440</p>
												</li>
												<li>
													<i class="features-icon thickness"></i>
													<p class="defult-font">Thickness ranging from 2.5rnm to 18mm </p>
												</li>
												<li>
													<i class="features-icon core"></i>
													<p class="defult-font">Core: Poplar, Combi , Hardwood, </p>
												</li>
												<li>
													<i class="features-icon grades"></i>
													<p class="defult-font">Grades : BB/CC , BB/BB </p>
												</li>
												<li>
													<i class="features-icon importing"></i>
													<p class="defult-font">Main importing countries: China, India, Malaysia.</p>
												</li>
											</ul>
										</div>
										<div class="row">
											<div class="heading">
											</div>
											<p class="defult-font">
												All Plmood products are accredited with Quality 
												<br><br>

												Certifications and other Standard Certifications. Products have been subjected to Quality Tests in Specific Laboratories in the relevant countries.
											</p>
										</div>
									</div>
									<!-- Slide 2 -->
									<div class="products-list" id="list-2">
										<div class="row">
											<div class="col-7">
												<h1>Commercial Plywood</h1>
												<p>
													Our Plywood sheets are manufactured in Modemized Factories and are of best Quality that can be used for residential & commercial furniture products and in the construction industry for numerous applications.
												</p>

												<a href="#" class="btn-defult">How it is Made</a>
											</div>
											<div class="col-5 image-cols">
												<img src="img/product.jpg" alt="">
											</div>
										</div>
										<div class="row">
											<div class="heading">
												<h6>Features</h6>
											</div>
											<ul class="features">
												<li>
													<i class="features-icon dimension"></i>
													<p class="defult-font">Common Dimension is 1220 X 2440</p>
												</li>
												<li>
													<i class="features-icon thickness"></i>
													<p class="defult-font">Thickness ranging from 2.5rnm to 18mm </p>
												</li>
												<li>
													<i class="features-icon core"></i>
													<p class="defult-font">Core: Poplar, Combi , Hardwood, </p>
												</li>
												<li>
													<i class="features-icon grades"></i>
													<p class="defult-font">Grades : BB/CC , BB/BB </p>
												</li>
												<li>
													<i class="features-icon importing"></i>
													<p class="defult-font">Main importing countries: China, India, Malaysia.</p>
												</li>
											</ul>
										</div>
										<div class="row">
											<div class="heading">
											</div>
											<p class="defult-font">
												All Plmood products are accredited with Quality 
												<br><br>

												Certifications and other Standard Certifications. Products have been subjected to Quality Tests in Specific Laboratories in the relevant countries.
											</p>
										</div>
									</div>
								</div>
							</div> 	
						</div>
					</div>
				</div>
			</div>
			<footer class="footer-section">
				<div class="container">
					<div class="row">
						<div class="col-4">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="footer-about">About</h3>
								<div class="containt-slide footer-about">
									<p>
										PUWAKARAMBA ENTERPRISES is an organization with a highly reputation of  importer and major distributor in Sri Lanka. We are mainly importing the Wood panel products such as 
										Plywood, MDF, Hardboard with all kind of Hardware items and Upholstery Fabric etc..
										<br><br>
										During these 15 years of business experience we have assembled valuable customer base country wide and as that we have supplied our products to all provincial areas.
									</p>
								</div>
							</div>
						</div>
						<div class="col-3">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="footer-about">Head Office</h3>
								<div class="containt-slide ">
									<p>
										Address: No 44 , De Soysa Road, <br>
										Rawathawatte, Moratuwa
										<br><br>
										Tel: 011- 2648047  , Fax : 011 2648048 <br>
										<br><br>
										E-mail: sandj@sltnet.lk <br>
										info@puwakarambaent.com
										<br><br>
										Show Room Address: Puwakaramba Enterprises,<br>
										Moratumulla South, Moratuwa, <br> Sri Lanka
										<br><br>
										Tel: 011-2654084 - 011-2653664 <br>
										Fax - 011-2648048
										<br><br>
										Warehouse
										<br><br>
										Address : No. 1 / 3 , Polhena ,Madapatha.
										<br><br>
										Tel: 0112706381 <br>
										Fax : 011 2706380
									</p>
								</div>
							</div>
						</div>
						<div class="col-2">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="">About</h3>
								<div class="containt-slide ">
									<ul class="social-icon">
										<li><a href="" target="_blank" class="icon-social icon-social-twitter"></a></li>
										<li><a href=""  target="_blank" class="icon-social icon-social-facebook"></a></li>
										<li><a href="#" class="icon-social icon-social-gplus"></a></li>
									</ul>

									<p>
										Terms and Conditions  
										Privacy  Disclaimer  
										Useful Links
									</p>

									<img class="qr-img" src="img/qr_code.jpg" alt="">

								</div>
							</div>
						</div>
						<div class="col-3">
							<div class="cols-rows">
								<h3 class="footer-click" data-click="">STAY IN TOUCH WITH</h3>
								<div class="containt-slide ">
									<p>
										Sign up for the Leisure Travel Vans newsletter and be the first to know about news, videos, product updates, and more. You can unsubscribe at any time.
									</p>
								</div>
							</div>
							<div class="cols-rows">
								<h3 class="footer-click" data-click="">Newsletter</h3>
								<div class="containt-slide ">
									<p>
										Signup for the monthly newsletter to get usefull information for your busi
									</p>

									<form class="subscribe" action="">
										<input type="text" placeholder="Email Address">
										<button>Sign up</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<span class="footer-line"></span>
			</footer>
		</div>
		<script type="text/javascript" src="js/dist/main.min.js"></script>
	</body>
</html>	